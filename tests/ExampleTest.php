<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Services_Twilio;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel 5')
             ->dontSee('Hello');
    }

    public function testPractice()
    {
        $a = 1;
        $b = -1;
        $this->assertEquals(1, $a);
        $this->assertNotEquals(1, $b);
    }

    public function testTwilioConnect()
    {


//        $client = new Services_Twilio($_ENV['TWILIO_ACCOUNT_SID'], $_ENV['TWILIO_AUTH_TOKEN']);
//        $this->assertNotEmpty($client);
//
//        $from = $client->account->incoming_phone_numbers->get("PN2d6bedb1bb685b19f7c5e67b347d47af") ;
//        $this->assertEquals(+18443948297, $from->phone_number);
//
//        $message = $client->account->messages->create(array(
//            "From" => $from->phone_number, // From a valid Twilio number
//            "To" => "+380985307521",   // Text this number
//            "Body" => "Hello from PHP",
//        ));
//        $this->assertNotEmpty($message->sid);
//        $this->assertEmpty($message->error_code);
//        $this->assertEquals('queued', $message->status);
//
//        $numbers = $client->account->available_phone_numbers->getList('DK', 'Local', array());
//        $this->assertCount(30, $numbers);
    }
}